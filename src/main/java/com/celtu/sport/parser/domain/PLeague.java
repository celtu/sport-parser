package com.celtu.sport.parser.domain;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PLeague implements Serializable {
	private String href;
	private String countryCode;
	private String countryName;
	private String name;
	private Integer order;

	public PLeague() {
	}

	public PLeague(String href, String countryName, String name, Integer order) {
		this.href = href;
		this.countryName = countryName;
		this.name = name;
		this.order = order;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "PLeague{" +
				"href='" + href + '\'' +
				", countryCode='" + countryCode + '\'' +
				", countryName='" + countryName + '\'' +
				", name='" + name + '\'' +
				", order=" + order +
				'}';
	}
}
