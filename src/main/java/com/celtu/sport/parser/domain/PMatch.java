package com.celtu.sport.parser.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@XmlRootElement
public class PMatch implements Serializable {
	
	private String externalId;
	
	private String homeName;
	private String awayName;
	
	private Integer homeScore;
	private Integer awayScore;
	
	private LocalDateTime date;

	private Status status;
	
	private List<POdds> oddsList;
	
	public String getHomeName() {
		return homeName;
	}
	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}
	public String getAwayName() {
		return awayName;
	}
	public void setAwayName(String awayName) {
		this.awayName = awayName;
	}
	public Integer getHomeScore() {
		return homeScore;
	}
	public void setHomeScore(Integer homeScore) {
		this.homeScore = homeScore;
	}
	public Integer getAwayScore() {
		return awayScore;
	}
	public void setAwayScore(Integer awayScore) {
		this.awayScore = awayScore;
	}
	public List<POdds> getOddsList() {
		return oddsList;
	}
	public void setOddsList(List<POdds> oddsList) {
		this.oddsList = oddsList;
	}
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}


	public enum Status {
		OPEN, CLOSED, POSTPONED, ABANDONED, WALKOVER;
	}
	
	
}
