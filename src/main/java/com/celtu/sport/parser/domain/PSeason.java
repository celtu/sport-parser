package com.celtu.sport.parser.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
public class PSeason implements Serializable {
	private String href;
	private String years;
	private Integer order;

	public PSeason() {
	}

	public PSeason(String href, String years, Integer order) {
		this.href = href;
		this.years = years;
		this.order = order;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getYears() {
		return years;
	}

	public void setYears(String years) {
		this.years = years;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}
}
