package com.celtu.sport.parser.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement
public class POdds implements Serializable {
	
	private String bookie;
	
	private BigDecimal home;
	private BigDecimal draw;
	private BigDecimal away;

	public String getBookie() {
		return bookie;
	}
	public void setBookie(String bookie) {
		this.bookie = bookie;
	}
	public BigDecimal getHome() {
		return home;
	}
	public void setHome(BigDecimal home) {
		this.home = home;
	}
	public BigDecimal getDraw() {
		return draw;
	}
	public void setDraw(BigDecimal draw) {
		this.draw = draw;
	}
	public BigDecimal getAway() {
		return away;
	}
	public void setAway(BigDecimal away) {
		this.away = away;
	}
	
}
