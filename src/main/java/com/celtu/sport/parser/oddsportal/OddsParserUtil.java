package com.celtu.sport.parser.oddsportal;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by celtu on 21.07.17.
 */
public enum OddsParserUtil {
    INSTANCE;

    private Pattern ODDS_PATTERN = Pattern.compile("\\d+(\\.\\d+)?");
    private Pattern NOT_RECENT_SEASON_HREF_PATTERN = Pattern.compile("\\d{4}/results");

    public BigDecimal readOdds(String oddsText) {
        if(oddsText == null) {
            return null;
        }
        Matcher matcher = ODDS_PATTERN.matcher(oddsText);
        if(matcher.find()) {
            String odds = matcher.group();
            BigDecimal oddsValue = new BigDecimal(odds);
            return oddsValue;
        }
        return null;
    }

    public boolean isTheMostRecentSeason(String seasonHref) {
        Matcher matcher = NOT_RECENT_SEASON_HREF_PATTERN.matcher(seasonHref);
        return !matcher.find();
    }
}
