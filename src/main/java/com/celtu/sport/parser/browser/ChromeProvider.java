package com.celtu.sport.parser.browser;

import com.celtu.sport.parser.SportParserConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;


public class ChromeProvider implements WebDriverProvider {

    private final static Logger logger = LoggerFactory.getLogger(ChromeProvider.class);

    private SportParserConfig config;

    public ChromeProvider(SportParserConfig config) {
        this.config = config;
    }

    @Override
    public WebDriver newDriver(boolean enableJavascript) {
        if (System.getProperty("webdriver.chrome.driver") == null) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chrome/chromedriver");
        }
        ChromeOptions chromeOptions = new ChromeOptions();
        if (config.isHeadless()) {
            chromeOptions.addArguments("--headless");
        }
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--proxy-bypass-list=*");
        chromeOptions.addArguments("--proxy-server='direct://'");
        chromeOptions.addArguments("--blink-settings=imagesEnabled=false");
        if (!enableJavascript) {
            chromeOptions.addArguments("--disable-javascript");
        }
//        ChromeDriver driver = new ChromeDriver(chromeOptions);
        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(new URL(config.getWebdriverUrl()), chromeOptions);
        } catch (MalformedURLException e) {
            logger.error("Remote chrome driver error.", e);
        }
        return driver;
    }

    @Override
    public WebDriver newDriver() {
        return newDriver(true);
    }
}
