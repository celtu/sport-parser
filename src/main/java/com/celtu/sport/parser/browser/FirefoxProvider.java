package com.celtu.sport.parser.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class FirefoxProvider implements WebDriverProvider {

    @Override
    // TODO: 13.04.2020 handle enable js for firefox if necessary
    public WebDriver newDriver(boolean enableJavascript) {
        return newDriver();
    }

    public WebDriver newDriver() {
        System.setProperty("webdriver.gecko.driver", "src/main/resources/firefox/geckodriver");
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setHeadless(true);
        FirefoxDriver driver = new FirefoxDriver(firefoxOptions);
        return driver;
    }
}
