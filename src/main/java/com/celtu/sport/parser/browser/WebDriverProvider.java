package com.celtu.sport.parser.browser;

import org.openqa.selenium.WebDriver;

public interface WebDriverProvider {
    WebDriver newDriver(boolean enableJavascript);

    WebDriver newDriver();
}
