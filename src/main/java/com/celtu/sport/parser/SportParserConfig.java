package com.celtu.sport.parser;

/* Created by celtu on 29.04.2020 */
public class SportParserConfig {
    private String webdriverUrl;
    private boolean headless;

    public SportParserConfig(String webdriverUrl) {
        this(webdriverUrl, true);
    }

    public SportParserConfig(String webdriverUrl, boolean headless) {
        this.webdriverUrl = webdriverUrl;
        this.headless = headless;
    }

    public static SportParserConfig defaultConfig() {
        SportParserConfig config = new SportParserConfig("http://localhost:9515");
        return config;
    }

    public String getWebdriverUrl() {
        return webdriverUrl;
    }

    public void setWebdriverUrl(String webdriverUrl) {
        this.webdriverUrl = webdriverUrl;
    }

    public boolean isHeadless() {
        return headless;
    }

    public void setHeadless(boolean headless) {
        this.headless = headless;
    }
}
