package com.celtu.sport.parser;

public class SportParserException extends RuntimeException {
    public SportParserException() {
    }

    public SportParserException(String message) {
        super(message);
    }

    public SportParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public SportParserException(Throwable cause) {
        super(cause);
    }

    public SportParserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
