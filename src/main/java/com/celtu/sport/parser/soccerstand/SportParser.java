package com.celtu.sport.parser.soccerstand;

import com.celtu.sport.parser.SportParserConfig;
import com.celtu.sport.parser.SportParserException;
import com.celtu.sport.parser.browser.ChromeProvider;
import com.celtu.sport.parser.browser.WebDriverProvider;
import com.celtu.sport.parser.domain.PLeague;
import com.celtu.sport.parser.domain.PMatch;
import com.celtu.sport.parser.domain.POdds;
import com.celtu.sport.parser.domain.PSeason;
import com.celtu.sport.parser.oddsportal.OddsParserUtil;
import com.celtu.sport.parser.util.WebDriverUtil;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SportParser {

    private final static Logger logger = LoggerFactory.getLogger(SportParser.class);
    public static final String BASE_URL = "https://www.soccerstand.com";
    public static final String ODDSPORTAL_BASE_URL = "https://www.oddsportal.com";

    private final static Pattern SEASON_NAME_YEARS_PATTERN = Pattern.compile(".*? ([/\\d]+)$");
    private final static Pattern LEAGUE_HREF_PATTERN = Pattern.compile("(/.*/)[a-z -]+/?");
    public static final Duration PAGE_LOAD_TIMEOUT = Duration.ofSeconds(30);
    public static final String XPATH_COUNTRY_NESTED_LEAGUE = ".//a[contains(@class,'lmc__templateHref')]";

    private WebDriverProvider webDriverProvider;
    private DateTimeFormatter matchDateFormat;

    public SportParser() {
        this(SportParserConfig.defaultConfig());
    }

    public SportParser(SportParserConfig config) throws SportParserException {
        System.setProperty("phantomjs.binary.path", "/home/celtu/phantomjs/phantomjs");
        this.webDriverProvider = new ChromeProvider(config);
        this.matchDateFormat = DateTimeFormatter.ofPattern("dd MMM yyyy,HH:mm", Locale.ENGLISH);
    }

    public boolean signInOddsportal() {
        logger.info("Login into OddsPortal.com");
        WebDriver driver = null;
        try {
            driver = webDriverProvider.newDriver();
            driver.navigate().to(ODDSPORTAL_BASE_URL + "/login/");
            WebDriverWait wait = new WebDriverWait(driver, PAGE_LOAD_TIMEOUT);
            WebElement username = driver.findElement(By.xpath("//input[@id='login-username-sign']"));
            username.sendKeys("fpemailer");
            WebElement password = driver.findElement(By.xpath("//input[@id='login-password-sign']"));
            password.sendKeys("rysujcie");
            WebElement submit = driver.findElement(By.xpath("//input[@name='login-submit']"));
            WebDriverUtil.click(driver, submit);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='loginModalBtn']")));
            logger.info("User login into OddsPortal.com successful");
//            driver.navigate().to(ODDSPORTAL_BASE_URL + "/set-timezone/37/");
//            new WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(By.id("user-header-timezone-expander")));
            Set<Cookie> cookies = driver.manage().getCookies().stream()
//                        .filter(cookie -> !cookie.getName().contains("zone"))
                    .collect(Collectors.toSet());
            OddsportalUserCookies.INSTANCE.set(cookies);
            return true;
        } finally {
            closeWebDriver(driver);
        }

    }

    public List<PLeague> parseLeagues() throws SportParserException {
        List<PLeague> leagues = new ArrayList<>();
        final WebDriver driver = webDriverProvider.newDriver();;
        try {
            driver.navigate().to(BASE_URL + "/soccer");
            WebElement elementMore = driver.findElement(By.xpath("//span[contains(@class, 'lmc__itemMore')]"));
            WebDriverUtil.click(driver, elementMore);
            WebElement countriesMenu = driver.findElement(By.xpath("//div[contains(@class, 'lmc__head')]//../.."));
            List<WebElement> countries = countriesMenu.findElements(By.xpath(".//div/a[contains(@href, '/soccer/')]/.."));

            countries.parallelStream().forEach(ce -> {
                WebElement countryLink = ce.findElement(By.xpath("./a"));
                WebDriverUtil.click(driver, countryLink);
            });

            countries.parallelStream().forEach(ce -> {
                WebDriverWait wait = new WebDriverWait(driver, PAGE_LOAD_TIMEOUT);
                wait.until(ExpectedConditions.attributeContains(ce, "class", "lmc__blockOpened"));
            });

            for (WebElement countryElement : countries) {
                String countryName = countryElement.findElement(By.xpath("./a/span")).getText();
                List<WebElement> countryLeagues = countryElement.findElements(By.xpath(".//a[contains(@class, 'lmc__templateHref')]"));
                int leagueOrder = 0;
                for (WebElement countryLeague : countryLeagues) {
                    String name = countryLeague.getText();
                    String href = countryLeague.getAttribute("href").replace(BASE_URL, "");
                    PLeague league = new PLeague(href, countryName, name, leagueOrder++);
                    leagues.add(league);
                }
            }
        } finally {
            closeWebDriver(driver);
        }
        return leagues;
    }

    private void closeWebDriver(WebDriver driver) {
        try {
            driver.quit();
        } catch (Exception e) {
            logger.warn("Problems while quitting WebDriver", e);
        }
    }

    private List<PLeague> parseSelectedCountryLeagues(WebDriver driver, WebElement countryElement) {
        WebDriverUtil.click(driver, countryElement);
        String countryName = countryElement.getText();
        WebElement countryParent = countryElement.findElement(By.xpath("./.."));
        new WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(ExpectedConditions.presenceOfNestedElementLocatedBy(countryParent, By.xpath(XPATH_COUNTRY_NESTED_LEAGUE)));
        List<PLeague> countryLeagues = new ArrayList<>();
        List<WebElement> leagueElements = countryParent.findElements(By.xpath(XPATH_COUNTRY_NESTED_LEAGUE));
        int order = 0;
        for (WebElement leagueElement : leagueElements) {
            String leagueHref = leagueElement.getAttribute("href").replaceFirst(BASE_URL, "");
            String leagueName = leagueElement.getText();
            PLeague pLeague = new PLeague(leagueHref, countryName, leagueName, order++);
            countryLeagues.add(pLeague);
        }
        return countryLeagues;
    }

    private List<PLeague> parseCountryLeagues(WebDriver driver, WebElement countryElement) {
        List<PLeague> countryLeagues = new ArrayList<>();
        String countryName = countryElement.getText();
        String countryHref = countryElement.getAttribute("href").replace(ODDSPORTAL_BASE_URL, "");
        List<WebElement> leagueElements = countryElement.findElements(By.xpath("//td/a[contains(@href,'" + countryHref + "')]"));
        int order = 0;
        for (WebElement leagueElement : leagueElements) {
            String leagueHref = leagueElement.getAttribute("href").replaceFirst(ODDSPORTAL_BASE_URL, "");
            String leagueName = leagueElement.getText();
            PLeague pLeague = new PLeague(leagueHref, countryName, leagueName, order++);
            countryLeagues.add(pLeague);
        }
        logger.info("Leagues of country {}: {}", countryName, countryLeagues);
        return countryLeagues;
    }

    public PLeague parseLeagueDetails(String leagueHref) throws SportParserException {
        WebDriver driver = null;
        try {
            driver = webDriverProvider.newDriver();
            driver.navigate().to(BASE_URL + leagueHref);
            WebDriverWait wait = new WebDriverWait(driver, PAGE_LOAD_TIMEOUT);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("preload")));
            WebElement showMoreElement = driver.findElement(By.xpath("//span[@class='lmc__itemMore']"));
            WebDriverUtil.click(driver, showMoreElement);
            new WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//ul[contains(@class, 'selected-country-list')]/li[@class='show-more']/a")));
            Matcher matcher = LEAGUE_HREF_PATTERN.matcher(leagueHref);
            matcher.find();
            String countryHref = matcher.group(1);
            WebElement countryElement = driver.findElement(By.xpath("//a[contains(@class, 'lmc__item') and @href='" + countryHref + "']"));
            List<PLeague> pLeagues = parseSelectedCountryLeagues(driver, countryElement);
            Optional<PLeague> first = pLeagues.stream().filter(l -> leagueHref.equals(l.getHref())).findFirst();
            if (first.isPresent()) {
                return first.get();
            } else {
                throw new SportParserException("Could not find league details, leagueHref: " + leagueHref);
            }
        } finally {
            closeWebDriver(driver);
        }
    }

    public List<PSeason> parseSeasons(String leagueHref) throws SportParserException {
        List<PSeason> seasons = new ArrayList<>();
        String archivePath = BASE_URL + leagueHref + "archive/";
        WebDriver driver = null;
        try {
            driver = webDriverProvider.newDriver();
            driver.navigate().to(archivePath);
            WebDriverWait wait = new WebDriverWait(driver, PAGE_LOAD_TIMEOUT);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("archive__season")));
            List<WebElement> seasonElements = driver.findElements(By.xpath("//a[contains(@class,'archive__text') and contains(@href, '/soccer/')]"));
            if (seasonElements != null) {
                int order = 0;
                for (WebElement seasonElement : seasonElements) {
                    String seasonName = seasonElement.getText();
                    Matcher yearsMatcher = SEASON_NAME_YEARS_PATTERN.matcher(seasonName);
                    if (yearsMatcher.matches()) {
                        String years = yearsMatcher.group(1);
                        String seasonHref = buildSeasonHref(leagueHref, years);
                        PSeason season = new PSeason(seasonHref, years, order++);
                        seasons.add(season);
                    } else {
                        throw new RuntimeException("Could not extractMatchesRootElement seasons years of input string: " + seasonName + ", leagueHref: " + leagueHref);
                    }
                }
            }
            return seasons;
        } finally {
            closeWebDriver(driver);
        }
    }

    private static String buildSeasonHref(String leagueHref, String years) {
        StringBuilder hrefBuilder = new StringBuilder(leagueHref);
        int lastCharIdx = hrefBuilder.length() - 1;
        if (hrefBuilder.charAt(lastCharIdx) == '/') {
            hrefBuilder.deleteCharAt(lastCharIdx);
        }
        hrefBuilder.append("-").append(years.replaceAll("/", "-")).append("/");
        return hrefBuilder.toString();
    }

    public List<String> parseRawMatches(String seasonHref) throws SportParserException {
        logger.info("Extract match external ids for season: {}", seasonHref);
        String seasonFullUrl = BASE_URL + seasonHref;
        List<String> rawMatches = new ArrayList<>();
        WebDriver driver = webDriverProvider.newDriver();
        try {
            List<String> resultsRawMatches = parseRawMatchesFromUrl(seasonFullUrl + "results", driver);
            List<String> fixturesRawMatches = parseRawMatchesFromUrl(seasonFullUrl + "fixtures", driver);
            rawMatches.addAll(resultsRawMatches);
            rawMatches.addAll(fixturesRawMatches);
        } finally {
            if (driver != null) {
                closeWebDriver(driver);
            }
        }
        logger.info("Extract match external ids for season: {} finished, extracted {} matches", seasonHref, rawMatches.size());
        return rawMatches;
    }

    private List<String> parseRawMatchesFromUrl(String url, WebDriver driver) throws SportParserException {
        logger.info("Parse matches from url: {}", url);
        driver.navigate().to(url);
        expand(driver);
        List<String> rawMatches = new ArrayList<>();
        List<WebElement> headers = driver.findElements(By.xpath("//div[contains(@class, 'event__header')]"))
                .stream()
                .filter(header ->
                        !StringUtils.containsAny(header.getText(), "Play Off", "Play Out", "Relegation", "Quadrangular", "Final", "Promotion", "rd place", "th place", "nd place", "st place", "Placement matches", "Additional match")
                        ||
                        StringUtils.containsAny(header.getText(), "Relegation Group", "Championship Group"))
                .collect(Collectors.toList());
        for (WebElement header : headers) {
            rawMatches.addAll(parseMatchesUnderHeader(header));
        }
        return rawMatches;
    }

    private Collection<String> parseMatchesUnderHeader(WebElement roundHeader) {
        List<String> headerMatches = new ArrayList<>();
        WebElement element = roundHeader;
        boolean matchFound;
        boolean roundHeaderFound;
        do {
            matchFound = false;
            roundHeaderFound = false;
            try {
                element = element.findElement(By.xpath("./following-sibling::div"));
                String aClass = element.getAttribute("class");
                if (aClass.contains("event__match")) {
                    String idAttr = element.getAttribute("id");
                    String externalId = StringUtils.substringAfterLast(idAttr, "_");
                    headerMatches.add(externalId);
                    matchFound = true;
                } else if (aClass.contains("event__round")) {
                    roundHeaderFound = true;
                }
            } catch (NoSuchElementException nsee) {
                logger.trace("End of reading matches under round", nsee);
                element = null;
            }
        } while (element != null && (matchFound || roundHeaderFound));
        return headerMatches;
    }

    private void expand(WebDriver driver) {
        logger.info("expand matches page - start");
        WebDriverWait wait = new WebDriverWait(driver, PAGE_LOAD_TIMEOUT);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("preload")));
        int counter = 0;
        try {
            while (true) {
                String showMoreXpath = "//a[contains(@class, 'event__more')]";
                WebElement showMore = driver.findElement(By.xpath(showMoreXpath));
                if (showMore != null) {
                    logger.debug("wait - start");
                    wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(@class, 'loadingOverlay')]")));
                    showMore = driver.findElement(By.xpath(showMoreXpath));
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(showMoreXpath)));
                    showMore = driver.findElement(By.xpath(showMoreXpath));
                    logger.debug("wait - end");
                    WebDriverUtil.click(driver, showMore);
                } else {
                    logger.info("dupa");
                }
                counter++;
            }
        } catch (NoSuchElementException e) {
            logger.debug("expanded " + counter + " times");
        }
        logger.debug("expand matches page - end");
    }

    public PMatch parseMatchDetails(String externalId) throws SportParserException {
        logger.info("Parsing match details for match externalId: {}", externalId);
        if (OddsportalUserCookies.INSTANCE.get() == null) {
            signInOddsportal();
        }
        WebDriver driver = null;
        try {
            driver = webDriverProvider.newDriver(true);
            if (OddsportalUserCookies.INSTANCE.get() != null) {
                driver.navigate().to(ODDSPORTAL_BASE_URL + "/notexistingone/");
//                new WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1")));
                WebDriver finalDriver = driver;
                OddsportalUserCookies.INSTANCE.get().stream().forEach(cookie -> finalDriver.manage().addCookie(cookie));
            }
            PMatch match = new PMatch();
            match.setExternalId(externalId);
            driver.navigate().to("https://www.oddsportal.com/soccer/x/x/x-x-" + externalId);
            By eventDateXpath = By.xpath("//div[contains(@class, 'bg-event-start-time')]/..");
            new WebDriverWait(driver, PAGE_LOAD_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(eventDateXpath));
            WebElement dateElement = driver.findElement(eventDateXpath);
            try {
                StringBuilder dateContentBuilder = new StringBuilder();
                dateContentBuilder.append(dateElement.findElement(By.xpath("./p[2]")).getText());
                dateContentBuilder.append(dateElement.findElement(By.xpath("./p[3]")).getText());
                String dateContent = dateContentBuilder.toString();
                LocalDateTime date = LocalDateTime.parse(dateContent, matchDateFormat);
                match.setDate(date);
            } catch (DateTimeParseException e) {
                logger.error("", e);
                throw new SportParserException("Could not parse date " + dateElement.getText() + " from href: " + externalId, e);
            }
            List<WebElement> teamElements = driver.findElements(By.xpath("//img[contains(@src, 'team-logo')]/../.."));
            if (teamElements.size() != 2) {
                throw new SportParserException("Expected 2 team elements of match, found " + teamElements.size());
            }
            WebElement homeElement = teamElements.get(0);
            String homeName = homeElement.findElement(By.xpath("./div/p")).getText();
            match.setHomeName(homeName);
            WebElement awayElement = teamElements.get(1);
            String awayName = awayElement.findElement(By.xpath("./div/p")).getText();
            match.setAwayName(awayName);
            String homeScoreContent = homeElement.findElement(By.xpath("./div[2]/div")).getText();

            // ODDS
            parseMatchOdds(externalId, driver, match);
            // STATUS
            PMatch.Status status = null;
            if (!StringUtils.isBlank(homeScoreContent)) {
                Integer homeScore = StringUtils.isBlank(homeScoreContent) ? null : Integer.parseInt(homeScoreContent);
                match.setHomeScore(homeScore);
                String awayScoreContent = awayElement.findElement(By.xpath("./div[2]/div")).getText();
                Integer awayScore = StringUtils.isBlank(awayScoreContent) ? null : Integer.parseInt(awayScoreContent);
                match.setAwayScore(awayScore);
                if (match.getHomeScore() != null && match.getAwayScore() != null) {
                    status = PMatch.Status.CLOSED;
                }
            } else {
                if (match.getDate().isBefore(LocalDateTime.now())) {
                    logger.debug("Match score content not found. Check whether is it walkover");
                    try {
                        WebElement walkoverElement = dateElement.findElement(By.xpath("./../div/div/strong[contains(text(), 'awarded')]"));
                        String walkoverAwardedWinner = walkoverElement.getText().replaceAll(" awarded", "");
                        if (homeName.contains(walkoverAwardedWinner)) {
                            match.setHomeScore(3);
                            match.setAwayScore(0);
                        } else if (awayName.contains(walkoverAwardedWinner)) {
                            match.setHomeScore(0);
                            match.setAwayScore(3);
                        }
                        status = PMatch.Status.WALKOVER;
                    } catch (NoSuchElementException nse) {
                        logger.debug("It is not walkover match - walkover exclamation not found");
                    }
                    if (status == null) {
                        logger.debug("Check postponed match on soccerstand");
                        driver.navigate().to(BASE_URL + "/pl/mecz/" + externalId);
                        try {
                            WebElement postponedMatchDetail = driver.findElement(By.xpath("//span[contains(text(), 'rzełożone')]"));
                            if (postponedMatchDetail != null) {
                                status = PMatch.Status.POSTPONED;
                            }
                        } catch (NoSuchElementException nse) {
                            logger.debug("It is not postponed match - postponed match text not found");
                        }
                    }
                    if (status == null) {
                        logger.debug("Check abandoned match");
                    }
                }
            }
            match.setStatus(Optional.ofNullable(status).orElse(PMatch.Status.OPEN));
            return match;
        } finally {
            closeWebDriver(driver);
        }
    }

    private void parseMatchOdds(String externalId, WebDriver driver, PMatch match) {
        try {
            driver.findElement(By.xpath("//ul/li[contains(@class, 'active-odds')]/span/div[contains(text(), '1X2')]"));
        } catch (NoSuchElementException nse) {
            logger.warn("Skip parsing odds - 1X2 odds not found for match external id: {}.", externalId);
            return;
        }
        List<POdds> oddsList = new ArrayList<>();
        match.setOddsList(oddsList);
        List<WebElement> bookmakersOdds = driver.findElements(By.xpath("//p[contains(text(), 'Bookmakers')]/../../../../div/div/a/img[@alt][@title]/../../.."));
        for (WebElement bookieOdds : bookmakersOdds) {
            try {
                POdds odds = parseOddsRow(bookieOdds);
                oddsList.add(odds);
            } catch (SportParserException spe) {
                logger.warn("Could not parse odds row: " + bookieOdds.getText(), spe);
            }
        }
    }

    private static POdds parseOddsRow(WebElement bookieOdds) {
        POdds odds = new POdds();
        try {
            String bookmakerName = bookieOdds.findElement(By.xpath("./div[1]/a/p")).getText();
            odds.setBookie(bookmakerName);
        } catch (Exception e) {
            throw new SportParserException("Could not resolve odds bookmaker name", e);
        }
        try {
            String oddsHomeContent = bookieOdds.findElement(By.xpath("(./div[2]/div/div/a | ./div[2]/div/div/p)")).getText();
            odds.setHome(OddsParserUtil.INSTANCE.readOdds(oddsHomeContent));
        } catch (Exception e) {
            throw new SportParserException("Could not resolve home odds", e);
        }
        try {
            String oddsDrawContent = bookieOdds.findElement(By.xpath("(./div[3]/div/div/a | ./div[3]/div/div/p)")).getText();
            odds.setDraw(OddsParserUtil.INSTANCE.readOdds(oddsDrawContent));
        } catch (Exception e) {
            throw new SportParserException("Could not resolve draw odds", e);
        }
        try {
            String oddsAwayContent = bookieOdds.findElement(By.xpath("(./div[4]/div/div/a | ./div[4]/div/div/p)")).getText();
            odds.setAway(OddsParserUtil.INSTANCE.readOdds(oddsAwayContent));
        } catch (Exception e) {
            throw new SportParserException("Could not resolve away odds", e);
        }
        return odds;
    }
}