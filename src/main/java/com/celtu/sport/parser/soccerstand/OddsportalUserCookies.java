package com.celtu.sport.parser.soccerstand;

import org.openqa.selenium.Cookie;

import java.util.Set;

public enum OddsportalUserCookies {
    INSTANCE;

    private Set<Cookie> cookies;

    public Set<Cookie> get() {
        return cookies;
    }

    public void set(Set<Cookie> cookies) {
        this.cookies = cookies;
    }
}
