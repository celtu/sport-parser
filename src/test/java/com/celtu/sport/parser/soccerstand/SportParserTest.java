package com.celtu.sport.parser.soccerstand;

import com.celtu.sport.parser.SportParserConfig;
import com.celtu.sport.parser.SportParserException;
import com.celtu.sport.parser.domain.PLeague;
import com.celtu.sport.parser.domain.PMatch;
import com.celtu.sport.parser.domain.POdds;
import com.celtu.sport.parser.domain.PSeason;
import com.celtu.sport.parser.soccerstand.configurator.LocalGuiChromeConfigurator;
import com.celtu.sport.parser.soccerstand.configurator.WebDriverConfigurator;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;
import java.util.regex.Pattern;

public class SportParserTest {

    private static SportParserConfig config;
    private static SportParser parser;
    private static WebDriverConfigurator configurator;

    static {
//        configurator = new DockerHeadlessChromeConfigurator();
        configurator = new LocalGuiChromeConfigurator();
        config = configurator.configure();
    }

    @BeforeClass
    public static void initParser() {
        parser = new SportParser(config);
    }

    @Test
    public void parseLeagues() {
        List<PLeague> leagues = parser.parseLeagues();
        Assertions.assertThat(leagues).isNotNull();
        Assertions.assertThat(leagues.size()).isGreaterThan(1000);
        Assertions.assertThat(leagues).filteredOn(pLeague -> pLeague.getHref().startsWith("/soccer/england/premier-league/"))
                .hasSize(1)
                .first()
                .hasFieldOrPropertyWithValue("href", "/soccer/england/premier-league/")
                .hasFieldOrPropertyWithValue("name", "Premier League")
                .hasFieldOrPropertyWithValue("countryName", "England")
                .hasFieldOrPropertyWithValue("order", 0);
        Assertions.assertThat(leagues).allMatch(pLeague -> pLeague.getHref().startsWith("/soccer/"));
    }

    @Test
    public void parseSeasons() {
        String leagueHref = "/soccer/england/premier-league/";
        List<PSeason> seasons = parser.parseSeasons(leagueHref);
        assertSeasons(seasons);
    }

    @Test
    public void parseSeasonsBrazil() {
        String leagueHref = "/soccer/brazil/serie-a/";
        List<PSeason> seasons = parser.parseSeasons(leagueHref);
        Pattern pattern = Pattern.compile("/soccer/brazil/serie-a-\\d{4}/");
        Assertions.assertThat(seasons).allMatch(s -> pattern.matcher(s.getHref()).matches());
    }

    private void assertSeasons(List<PSeason> seasons) {
        Assert.assertNotNull(seasons);
        Assert.assertTrue(seasons.size() > 10);
        for (PSeason season : seasons) {
            assertSeason(season);
        };
    }

    private void assertSeason(PSeason season) {
        Assert.assertNotNull(season.getHref());
        Assert.assertTrue(season.getYears().matches("\\d+/\\d{4}$"));
        Assertions.assertThat(season.getHref()).doesNotStartWith(SportParser.BASE_URL);
        Assertions.assertThat(season.getHref()).startsWith("/soccer");
        Assertions.assertThat(season.getHref()).matches(Pattern.compile("/soccer/[\\w-]+/[\\w-]+\\d{4}-\\d{4}/"));
    }

    @Test
    public void parseRawMatchesArchivedSeasonTest(){
        String seasonHref = "/soccer/italy/serie-b-2016-2017/";
        List<String> matches = parser.parseRawMatches(seasonHref);
        Assert.assertNotNull(matches);
        Assert.assertEquals(462, matches.size());
    }
    @Test
    @Ignore("This test is moment dependent hmmm")
    public void parseRawMatchesCurrentSeasonTest(){
        String seasonHref = "/soccer/poland/ekstraklasa/";
        List<String> matches = parser.parseRawMatches(seasonHref);
        Assert.assertNotNull(matches);
        Assert.assertEquals(240, matches.size());
    }

    @Test
    public void parseRawMatchesBeforePLSeasonTest(){
        String seasonHref = "/soccer/england/premier-league-2018-2019/";
        List<String> matches = parser.parseRawMatches(seasonHref);
        Assert.assertNotNull(matches);
        Assert.assertEquals(380, matches.size());
    }

    @Test
    public void parseMatchDetails()  {
        PMatch match = parser.parseMatchDetails("0W03yGP1");
        junit.framework.Assert.assertNotNull(match);
        Assert.assertEquals("Chelsea", match.getHomeName());
        Assert.assertEquals("Middlesbrough", match.getAwayName());
        Assert.assertEquals(Integer.valueOf(3), match.getHomeScore());
        Assert.assertEquals(Integer.valueOf(0), match.getAwayScore());
        Assertions.assertThat(match.getDate()).isEqualTo("2017-05-08T21:00:00");
        Assert.assertEquals(PMatch.Status.CLOSED, match.getStatus());
        List<POdds> odds = match.getOddsList();
        Assertions.assertThat(odds.size()).isGreaterThan(20);
        Assertions.assertThat(match.getExternalId()).isEqualTo("0W03yGP1");
    }

    @Test
    public void paraseFutureMatch() {
        PMatch match = parser.parseMatchDetails("Iw2KgAoU");
    }

    @Test
    public void testParseLiverpoolBurnleyDate() throws Exception {
        PMatch pMatch = parser.parseMatchDetails("xQaRWaqm");
        Assertions.assertThat(pMatch).isNotNull();
    }

    @Test
    public void testParseEredivisie() throws SportParserException {
        String seasonHref = "/soccer/netherlands/eredivisie-2015-2016/";
        List<String> matches = parser.parseRawMatches(seasonHref);
        Assert.assertNotNull(matches);
        Assert.assertEquals(306, matches.size());
    }



    @Test
    public void testParseLeagueDetails() throws SportParserException {
        PLeague league = parser.parseLeagueDetails("/soccer/italy/serie-a/");
        junit.framework.Assert.assertNotNull(league);
        junit.framework.Assert.assertEquals("Serie A", league.getName());
        junit.framework.Assert.assertEquals("Italy", league.getCountryName());
        junit.framework.Assert.assertEquals("/soccer/italy/serie-a/", league.getHref());

        league = parser.parseLeagueDetails("/soccer/england/premier-league/");
        junit.framework.Assert.assertNotNull(league);
        junit.framework.Assert.assertEquals("Premier League", league.getName());
        junit.framework.Assert.assertEquals("England", league.getCountryName());
        junit.framework.Assert.assertEquals("/soccer/england/premier-league/", league.getHref());

        league = parser.parseLeagueDetails("/soccer/poland/ekstraklasa/");
        junit.framework.Assert.assertNotNull(league);
        junit.framework.Assert.assertEquals("Ekstraklasa", league.getName());
        junit.framework.Assert.assertEquals("Poland", league.getCountryName());
        junit.framework.Assert.assertEquals("/soccer/poland/ekstraklasa/", league.getHref());

    }

    @Test
    public void testParseWalkower() throws SportParserException {
        PMatch match = parser.parseMatchDetails("hOSLMRP4");
        junit.framework.Assert.assertNotNull(match);
        junit.framework.Assert.assertEquals(Integer.valueOf(0), match.getHomeScore());
        junit.framework.Assert.assertEquals(Integer.valueOf(3), match.getAwayScore());
        junit.framework.Assert.assertEquals(PMatch.Status.WALKOVER, match.getStatus());
    }

    @Test
    public void parseLiveLegia() throws SportParserException {
        String matchHref = "Wtpd9YUo";
        testMatchParsing(matchHref);
    }

    @Test
    public void testParseDateEmpty() throws SportParserException {
        String matchHref = "ni58O6mJ";
        testMatchParsing(matchHref);
    }

    @Test
    public void testParseWithEmptyRedbetOdds() throws SportParserException {
        testMatchParsing("EFOFhegl");
    }

    @Test
    public void testParseWithParenthesis() throws SportParserException {
        testMatchParsing("KjF6FiA6");
    }

    private void testMatchParsing(String matchHref) throws SportParserException {
        PMatch match = parser.parseMatchDetails(matchHref);
        junit.framework.Assert.assertNotNull(match);
    }

    @Test
    public void testAbandonedMatch() {
        // TODO: 14.02.2023 brak w bazie
    }

    @Test
    @Ignore("Ulepszyć ten test, żeby wyszukał dynamicznie jakiś przełożony mecz")
    public void shouldResolvePostponedMatch() {
        // TODO: 14.02.2023 jak będzie jakiś przełożony to zaimplementujemy
        PMatch match = parser.parseMatchDetails("j1Ty0UIn");
        Assertions.assertThat(match.getStatus()).isEqualTo(PMatch.Status.POSTPONED);
    }

    @Test
    public void shouldParseMatchWithout1x2Odds() {
        PMatch match = parser.parseMatchDetails("6ZbpSKIB");
    }

    @Test
    public void shouldParseAllAustraliaA20202021Matches() {
        List<String> matches = parser.parseRawMatches("/soccer/australia/a-league-2020-2021/");
        Assertions.assertThat(matches).hasSize(156);
    }

    @Test
    public void shouldSkipPlayOffMatches() {
        List<String> matches = parser.parseRawMatches("/soccer/china/super-league-2020/");
        Assertions.assertThat(matches).hasSize(112)
                .doesNotContain("OlQFWYM7", "ClVZuw0L", "CA6AhGqR", "MBpFFRkn", "YaHZBQQc", "GO6vBpt4");
    }

    @Test
    public void shouldSkipPlayOffButKeepChampionshipGroupAndRelegationGroupMatches() {
        List<String> matches = parser.parseRawMatches("/soccer/china/super-league-2021/");
        Assertions.assertThat(matches).hasSize(176)
                .doesNotContain("0UgBrWs6", "vF52phDg", "I546qCSa", "Au1boYcm");
    }
    @Test
    public void shouldSkipPlacementMatches() {
        List<String> matches = parser.parseRawMatches("/soccer/costa-rica/primera-division-2020-2021/");
        Assertions.assertThat(matches).hasSize(228)
                .doesNotContain("08x2uYf8", "b3bJGlI7");
    }

    @Test
    public void shouldSkipPlayOutMatches() {
        List<String> matches = parser.parseRawMatches("/soccer/italy/serie-b-2018-2019//");
        Assertions.assertThat(matches).hasSize(342)
                .doesNotContain("nslZLTNk", "8jkVM98q");
    }

    @Test
    public void shouldParseAllPeruLiga12018Matches() {
        List<String> matches = parser.parseRawMatches("/soccer/peru/liga-1-2018/");
        Assertions.assertThat(matches).hasSize(352);
    }

    @Test
    public void shouldParseZvijezdSlobodaIgnoreMissingBet365Odds() {
        PMatch pm = parser.parseMatchDetails("OGKEbdON");
        Assertions.assertThat(pm.getOddsList())
                .matches(odds -> odds.size() > 10, "More odds than 10")
                .filteredOn(o -> o.getBookie().equals("bet365"))
                .hasSize(0);
    }

    @Test
    public void parseOpenMatchStatus() {
        PMatch match = parser.parseMatchDetails("xjepi2pl");
        Assertions.assertThat(match.getStatus()).isEqualByComparingTo(PMatch.Status.OPEN);
    }

    @Test
    public void shouldSkipAdditionalMatch() {
        List<String> matches = parser.parseRawMatches("/soccer/argentina/primera-nacional-2017-2018/");
        Assertions.assertThat(matches).hasSize(300).doesNotContain("fcA8s0Ae");
    }

}