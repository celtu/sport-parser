package com.celtu.sport.parser.soccerstand.configurator;

import com.celtu.sport.parser.SportParserConfig;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocalGuiChromeConfigurator implements WebDriverConfigurator {

    private ChromeDriver driver;

    @Override
    public SportParserConfig configure() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chrome/chromedriver");
        SportParserConfig config = new SportParserConfig("http://localhost:9515", false);
        return config;
    }

    @Override
    public void close() {
        // TODO: 05.12.2020 na ten moment to, że odpalimy chromedrivera sobie ręcznie na localu jest ok nie ma ryzyka, że zostawimy zombiaka
    }
}
