package com.celtu.sport.parser.soccerstand.configurator;

import com.celtu.sport.parser.SportParserConfig;

public interface WebDriverConfigurator {
    SportParserConfig configure();
    void close();
}
