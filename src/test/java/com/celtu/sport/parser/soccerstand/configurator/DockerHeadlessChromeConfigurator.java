package com.celtu.sport.parser.soccerstand.configurator;

import com.celtu.sport.parser.SportParserConfig;
import com.celtu.sport.parser.soccerstand.SportParserTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;

import java.io.File;
import java.time.Duration;

public class DockerHeadlessChromeConfigurator implements WebDriverConfigurator {

    private final static Logger logger = LoggerFactory.getLogger(DockerHeadlessChromeConfigurator.class);
    private DockerComposeContainer composeContainer;

    @Override
    public SportParserConfig configure() {
        logger.info("");
        composeContainer = new DockerComposeContainer(new File(SportParserTest.class.getClassLoader().getResource("docker/chromedriver/docker-compose.yml").getFile()))
                .withExposedService("chromedriver", 9515, Wait.forListeningPort().withStartupTimeout(Duration.ofSeconds(30)));
        composeContainer.start();
        SportParserConfig config = new SportParserConfig("http://localhost:15001");
        return config;
    }

    @Override
    public void close() {
        // should close as compose container closes
    }
}
